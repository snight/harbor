# Harbor 部署指南

# 实验环境
|主机名|    IP地址   |  端口  |  用途  |
|------|:----------:|:------:|-------|
|Harbor|10.10.51.193| 80,443 |镜像仓库|

# 一、签发私有证书
## 1、生成CA证书私钥
创建证书存放目录
```shell
mkdir -p /etc/harbor/ssl && cd /etc/harbor/ssl
```
使用openssl工具生成一个RSA私钥
```shell
$ openssl genrsa -des3 -out harbor.key 2048
Generating RSA private key, 2048 bit long modulus
.......................+++
......+++
e is 65537 (0x10001)
Enter pass phrase for harbor.key:        # 输入一个至少4位的密码
Verifying - Enter pass phrase for harbor.key:   # 重复输入密码
req：产生证书签发申请命令
```
- -x509：签发X.509格式证书命令，X.509是最通用的一种签名证书格式。
-  -new：生成证书请求
- -key：指定私钥文件
- -nodes：表示私钥不加密
- -out：输出
- -subj：指定用户信息
- -days：有效期

删除harbor.key中的密码
```shell
$ openssl rsa -in harbor.key -out harbor.key
Enter pass phrase for harbor.key:         # 输入刚才创建时的密码
writing RSA key
```
## 2、生成CSR（证书签名请求）
```shell
$ openssl req -new -key harbor.key -out harbor.csr
```
```
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:cn             # 国家
State or Province Name (full name) []:Shandong    # 地区 
Locality Name (eg, city) [Default City]:Jinan  # 城市
Organization Name (eg, company) [Default Company Ltd]:Telchina  # 组织
Organizational Unit Name (eg, section) []:IT  # 组织单位
Common Name (eg, your name or your server's hostname) []:Harbor    # 常用名可填自己名字或域名
Email Address []:syslog@telchina.net                         # 邮件地址

Please enter the following 'extra' attributes
to be sent with your certificate request      
A challenge password []:       # 可留空
An optional company name []:   # 可留空
```
## 3、生成自签名证书
注意：在使用自签名的临时证书时，浏览器会提示证书的颁发机构是未知的。
```shell
$ echo subjectAltName = IP:10.10.51.193 > extfile.cnf
$ openssl x509 -req -days 36500 -in harbor.csr -signkey harbor.key -out harbor.crt -extfile extfile.cnf
```
```
Signature ok
subject=/C=CN/ST=SD/L=JN/O=CO/OU=CO/CN=CO/emailAddress=test@harbor.com
Getting Private key
```
- x509：签发X.509格式证书命令。
- -req：表示证书输入请求。
- -days：表示有效天数
- -extensions：表示按OpenSSL配置文件v3_req项添加扩展。
- -CA：表示CA证书,这里为ca.crt
- -CAkey：表示CA证书密钥,这里为ca.key
- -CAcreateserial：表示创建CA证书序列号
- -extfile：指定文件
 
# 二、准备环境
## 1、安装常用软件包
```
yum install -y vim bash-completion net-tools gcc socat wget epel-release yum-utils device-mapper-persistent-data lvm2
```
## 2、关闭selinux
```shell
setenforce 0
sed -i "s|SELINUX=enforcing|SELINUX=disabled|" /etc/selinux/config
```
## 3、关闭防火墙
```
systemctl stop firewalld && systemctl disable firewalld
```
## 4、安装docker-ce
```shell
yum-config-manager --add-repo https://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
yum -y install docker
systemctl enable docker
```
## 5、安装docker-compose
```
yum -y install docker-compose
```
# 三、安装Harbor数据库
## 1、下载项目代码到/opt目录
```
git clone https://gitlab.com/snight/harbor.git /opt/
```
## 2、启动数据库容器
```shell
cd /opt/harbor/database
docker-compose up -d
```
## 3、查看数据库运行状态
```
docker ps -a
```
# 四、安装Harbor
## 1、提供证书给Docker
```shell
# 如果如下目录不存在，请创建，如果有域名请按此格式依次创建
# mkdir -p /etc/docker/certs.d/<IP>
# mkdir -p /etc/docker/certs.d/<DOMAIN NAME>
# 如果端口为443，则不需要指定。如果为自定义端口，请指定端口
# mkdir -p /etc/docker/certs.d/yourdomain.com:port

mkdir -p /etc/docker/certs.d/10.10.51.193

# 将ca根证书依次复制到上述创建的目录中
cp /etc/harbor/ssl/harbor.crt /etc/docker/certs.d/10.10.51.193/

# 重启 Docker
service docker restart
```
## 2、配置harbor.yml文件
- hostname：主机域名或iP地址
- https：配置证书
- external_database：配置外部PostgreSQL数据库的参数
- external_redis：配置外部Redis数据库参数

```yaml
vim /opt/harbor/harbor.yml
...
hostname: 10.10.51.193

# http related config
http:
  # port for http, default is 80. If https enabled, this port will redirect to https port
  port: 80

# https related config
https:
  # https port for harbor, default is 443
  port: 443
  # The path of cert and key files for nginx
  certificate: /etc/harbor/ssl/server.crt
  private_key: /etc/harbor/ssl/server.key
...
external_database:
  harbor:
    host: 10.10.51.193
    port: 20001
    db_name: registry
    username: postgres
    password: root123
    ssl_mode: disable
    max_idle_conns: 2
    max_open_conns: 0
  clair:
    host: 10.10.51.193
    port: 20002
    db_name: clair
    username: postgres
    password: root123
    ssl_mode: disable
  notary_signer:
    host: 10.10.51.193
    port: 20003
    db_name: notarysigner
    username: postgres
    password: root123
    ssl_mode: disable
  notary_server:
    host: 10.10.51.193
    port: 20004
    db_name: notaryserver
    username: postgres
    password: root123
    ssl_mode: disable
external_redis:
  host: 10.10.51.193:20000
  password:
  registry_db_index: 1
  jobservice_db_index: 2
  chartmuseum_db_index: 3
  clair_db_index: 4
  trivy_db_index: 5
  idle_timeout_seconds: 30
...
```
## 3、为Harbor生成配置文件
```shell
cd /opt/harbor
./prepare
```
## 4、安装启动Harbor
```
./install.sh
```
## 5、删除/重启Harbor
如果Harbor正在运行，或者需要更改harbor配置，必须停止并删除现有实例，然后重启Harbor
```shell
# 返回Harbor的安装目录
cd /opt/harbor
# 删除现有Harbor实例
docker-compose down -v
# 重启 Harbor
docker-compose up -d
# 启动 Harbor
docker-compose start
# 停止 Harbor
docker-compose stop
```
## 6、创建Harbor系统服务
```shell
cat > /usr/lib/systemd/system/harbor.service << EOF
[Unit]
Description=Harbor
After=docker.service systemd-networkd.service systemd-resolved.service
Requires=docker.service
Documentation=http://github.com/vmware/harbor

[Service]
Type=simple
Restart=on-failure
RestartSec=5
ExecStart=/usr/bin/docker-compose -f /opt/harbor/docker-compose.yml up
ExecStop=/usr/bin/docker-compose -f /opt/harbor/docker-compose.yml down

[Install]
WantedBy=multi-user.target
EOF
```

